$(document).ready(function(){

	/* $('fieldset input[type="submit"]').hide(); */

	/* Function pulls the "data-" attribute from the attached rado button, then parses the data types to create a show and hide function. Additional commands can be created by adding more "data-" attributes to the radio inputs, and parsing as extra parts of the function. */

	$.fn.showOptions = function(){
	    var data = $(this).data();
	  	$(data.hide).hide();
	    $(data.show).show();
	};

	/* 'When a radio button is clicked, if the name is "optSelect" and it is checked, run the attached function. */
	$('fieldset input[type="radio"]').click(function() {
		$('input[name="optSelect"]:checked').showOptions();
	});

	/* Function version one, obsolete
	$('fieldset input[type="radio"]').click(function() {
		if($('input[name="initial"]:checked').val()) {
			// alert('something is selected!');
			if ($(this).val() == 'courses') {
				$('.first').hide();
				$('.subj.init').show();
			}
			if ($(this).val() == 'campus') {
				$('.first').hide();
				$('.about.init').show();
			}
			if ($(this).val() == 'media') {
				$('.first').hide();
				$('.media').show();
			}
			if ($(this).val() == 'support') {
				$('.first').hide();
				$('.support').show();
			}
			if ($(this).val() == 'apply') {
				$('.first').hide();
				$('.apply').show();
			}
		}
	});
	*/

	/* Function version two part one, obsolete
	$.fn.showOptions = function(targetValue, hideOpt, showOpt) {
		if ($(this).val() == targetValue) {
			$(hideOpt).hide();
			$(showOpt).show();
		}
	};

	Function version two part two, obsolete
	$('fieldset input[type="radio"]').click(function() {
		$('input[name="initial"]:checked').showOptions('courses', '.first', '.subj.init');
		$('input[name="initial"]:checked').showOptions('campus', '.first', '.about.init');
		$('input[name="initial"]:checked').showOptions('media', '.first', '.media');
		$('input[name="initial"]:checked').showOptions('support', '.first', '.support');
		$('input[name="initial"]:checked').showOptions('apply', '.first', '.apply');
	});
	*/
});